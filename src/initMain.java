import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jxl.read.biff.BiffException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class initMain {

	public static void main(String[] args) throws BiffException, IOException {

		readFromXls readXls = new readFromXls();
		//
		String pathForXlsFile = "/home/alka/Documents/QAMarketing/UrlsForResponsive.xlsx";
		String resultantXls= "/home/alka/Documents/QAMarketing/resultOfResponsiveTest.xlsx";
		
		ArrayList<String> urlsForTest = readXls.readExcel(pathForXlsFile, "Sheet1");
		HashMap<String, String> resultForResponsive = new HashMap<String, String>();

		for (String url : urlsForTest) {
			WebDriver driver = new FirefoxDriver();
			driver.get(url);
			driver.manage().window().maximize();
			String winHandleBefore = driver.getWindowHandle();
			driver.findElement(By.cssSelector("body")).sendKeys(
					Keys.chord(Keys.CONTROL, "u"));

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
			}
			// Check if the page is responsive i.e check for the below mentioned css in javascript
			if (driver.getPageSource().contains("bootstrap.min.css")
					&& driver.getPageSource().contains(
							"fusion_mainlp_style.css")) {
				System.out.println(url + " is responsive");
				// Put the url as key and the value will be a string claiming if it is Responsive or not
				resultForResponsive.put(url, "Responsive");
			} else {
				System.out.println(url + " is not responsive");
				resultForResponsive.put(url, "NOT Responsive");
			}
			
			//writeXls.writeResultToXls(resultantXls,resultForResponsive);
			driver.close();
			driver.switchTo().window(winHandleBefore);

		}

	}

}
